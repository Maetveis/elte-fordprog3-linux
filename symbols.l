%option noyywrap c++ yylineno

%{

#include <iostream>
#include <cstdlib>
#include "Parserbase.h"
%}

DIGIT   [0-9]
LETTER [a-zA-Z]
WS  [ \t\n]

%%

int 		return Parser::INT;
main 		return Parser::MAIN;
unsigned	return Parser::UNSIGNED;
bool 		return Parser::BOOL;
true 		return Parser::TRUE;
false 		return Parser::FALSE;
if 			return Parser::IF;
else 		return Parser::ELSE;
while 		return Parser::WHILE;
cout 		return Parser::COUT;
cin 		return Parser::CIN;

0|([1-9]{DIGIT}*) return Parser::NUMBER;

\/\/.* 		//Komment nem csinálunk semmit

\(		return Parser::POPEN;
\)		return Parser::PCLOSE;
\{		return Parser::BOPEN;
\}		return Parser::BCLOSE;

{LETTER}[a-zA-Z0-9_]*	return Parser::ID;

; 		return Parser::END;

! 		return Parser::NOT;

\>\>	return Parser::STREAM_IN;
\<\< 	return Parser::STREAM_OUT;
&& 		return Parser::AND;
\|\| 	return Parser::OR;
\<  	return Parser::LT;
\>		return Parser::GT;
\<=		return Parser::LE;
\>=		return Parser::GE;
==		return Parser::EQUAL;
\+		return Parser::PLUS;
-		return Parser::MINUS;
\/		return Parser::DIV;
\*		return Parser::TIMES;
\%		return Parser::MOD;
= 		return Parser::LET;


{WS}+       // feher szokozok: semmi teendo

.           {
                std::cerr << lineno() << ": Lexikalis hiba." << std::endl;
                exit(1);
            }
%%
