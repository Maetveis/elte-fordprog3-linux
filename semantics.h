#ifndef SEMANTICS_H
#define SEMANTICS_H

enum class ExpressionType
{
	Unsigned,
	Bool
};

struct ExpressionSemantics
{
	unsigned lineNo;
	ExpressionType type;

	ExpressionSemantics() = default;

	ExpressionSemantics(unsigned l, ExpressionType t) :
		lineNo(l),
		type(t)
	{
	}
};

#endif
